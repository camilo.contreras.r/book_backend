let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url= 'http://localhost:3000';


describe('Insert a book: ',()=>{

	it('should insert a book', (done) => {
		chai.request(url)
			.post('/api/books')
			.send({
      title: 'Test',
      isbn:'ISBN',
      author:'TEST',
      description:'TEST',
      published_date:'04-11-2019',
      publisher:'Editorial TEST'
    })
			.end( function(err,res){
				console.log(res.body)
				console.log(res.status)
				expect(res).to.have.status(200);
				done();
			});
	});
});
